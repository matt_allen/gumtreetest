package com.gumtree.test.data

import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val dataModule = module {
    single { SearchRepository(get(), Dispatchers.IO) }
}
