package com.gumtree.test.data

enum class Manufacturer(val apiName: String) {
    NISSAN("nissan"),
    BMW("bmw"),
    LEXUS("lexus"),
    TOYOTA("toyota"),
    FORD("ford"),
    RENAULT("renault"),
}
