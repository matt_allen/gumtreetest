package com.gumtree.test.data

data class SearchResult(
    val id: String,
    val name: String,
    val title: String,
    val make: String,
    val model: String,
    val price: String,
)
