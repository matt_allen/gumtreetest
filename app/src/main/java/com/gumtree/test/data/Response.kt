package com.gumtree.test.data

class Response<T>(val state: State, val data: T? = null, val error: Throwable? = null) {
    enum class State { LOADING, SUCCESS, FAILURE }

    companion object {
        fun <T> success(data: T?) = Response(State.SUCCESS, data)
        fun <T> loading() = Response<T>(State.LOADING)
        fun <T> error(error: Throwable?) = Response<T>(State.FAILURE, null, error)
    }
}
