package com.gumtree.test.data

import com.gumtree.test.api.GumtreeApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlin.coroutines.CoroutineContext

class SearchRepository(private val api: GumtreeApi, private val ioDispatcher: CoroutineContext) {
    fun search(make: Manufacturer, model: String, year: Int) = flow<Response<List<SearchResult>>> {
        emit(Response.loading())
        api.runCatching { search(make.apiName, model, year) }
            .onFailure { emit(Response.error(it)) }
            .onSuccess { emit(Response.success(it.searchResults.map { sr -> sr.toSearchResult() })) }
    }.flowOn(ioDispatcher)
}
