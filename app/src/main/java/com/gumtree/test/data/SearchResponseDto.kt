package com.gumtree.test.data

import kotlinx.serialization.Serializable

@Serializable
data class SearchResponseDto(
    val searchResults: List<SearchResultDto>,
)

@Serializable
data class SearchResultDto(
    val id: String,
    val name: String,
    val title: String,
    val make: String,
    val model: String,
    val price: String,
) {
    fun toSearchResult(): SearchResult {
        return SearchResult(id, name, title, make, model, price)
    }
}
