package com.gumtree.test.view

import com.gumtree.test.view.results.SearchResultsViewModel
import com.gumtree.test.view.search.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModule = module {
    viewModel { SearchResultsViewModel(get()) }
    viewModel { SearchViewModel() }
}
