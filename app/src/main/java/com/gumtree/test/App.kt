package com.gumtree.test

import android.app.Application
import com.gumtree.test.api.apiModule
import com.gumtree.test.data.dataModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(apiModule, dataModule)
        }
    }
}
