package com.gumtree.test.api

import com.gumtree.test.data.SearchResponseDto
import retrofit2.http.GET
import retrofit2.http.Path

interface GumtreeApi {
    @GET("search?make={make}&model={model}&year={year}")
    suspend fun search(
        @Path("make") make: String,
        @Path("model") model: String,
        @Path("year") year: Int,
    ): SearchResponseDto
}
