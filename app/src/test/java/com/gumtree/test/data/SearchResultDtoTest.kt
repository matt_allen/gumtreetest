package com.gumtree.test.data

import junit.framework.TestCase.assertEquals
import org.junit.Test

class SearchResultDtoTest {
    @Test
    fun testSearchResultDto_toSearchResult() {
        val result = SearchResultDto(
            "id", "name", "title", "make", "model", "price",
        )
        assertEquals(result.id, result.toSearchResult().id)
        assertEquals(result.name, result.toSearchResult().name)
        assertEquals(result.title, result.toSearchResult().title)
        assertEquals(result.make, result.toSearchResult().make)
        assertEquals(result.model, result.toSearchResult().model)
        assertEquals(result.price, result.toSearchResult().price)
    }
}
